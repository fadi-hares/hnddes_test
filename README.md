# hnddes_test

login screen:

- when you open the app sign in page will appear, username and password is already hard coded just click login.
- a dialog will popup showing a circle progress indicator
- if any error occurs an error snackbar will display from the bottom of the screen
- if login succeeded you'll navigate to the second screen and the response data will stored locally

second screen:

- two api calls will requested
- while getting data a loading state will display
- if any error happen an error state will display and error text will appear in the center of the screen
- if the request succeeded the data will showup and you can start scrolling
- each time you reach the end of the page a new api call will happen and a new data will be added to the existing one in the state
- a circle indictor will showup at the bottom while loading a new data
- any error happen an error text widget will appear at the bottom of the list view
- if you reach the last page of the api call you'll see a widget text telling you there's no more data to fetch and you can't make any new calls to the end point.
- there's a timer set to 15 minutes with a callback func that will push you back to login screen when it's finish

NOTE:

- I couldn't find types of leaves in the response
