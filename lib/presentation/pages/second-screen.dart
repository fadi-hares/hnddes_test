import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:hnddes_test/app/app-color.dart';

import '../../app/router.dart';
import '../../controllers/home/home_bloc.dart';
import '../../controllers/home/home_state.dart';
import '../widgets/annual-leaves-curve.dart';
import '../widgets/leave-widget.dart';

class SecondScreen extends StatefulWidget {
  const SecondScreen({super.key});

  @override
  State<SecondScreen> createState() => _SecondScreenState();
}

class _SecondScreenState extends State<SecondScreen> {
  late ScrollController _controller;

  @override
  void initState() {
    super.initState();
    Timer(
      const Duration(minutes: 15),
      () => Navigator.pushReplacementNamed(
        context,
        Routs.loginScreen,
      ),
    );

    _controller = ScrollController()
      ..addListener(() {
        if (_controller.offset == _controller.position.maxScrollExtent) {
          context.read<HomeBloc>().add(const GetLeaveList());
        }
      });

    context.read<HomeBloc>().add(const GetInitData());
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
        backgroundColor: Colors.white,
        centerTitle: true,
        title: Text(
          'My leaves',
          style: TextStyle(
            color: Colors.black,
            fontSize: 15.sp,
          ),
        ),
        actions: [
          Padding(
            padding: EdgeInsets.symmetric(horizontal: 10.w),
            child: Icon(
              CupertinoIcons.profile_circled,
              color: AppColor.mainColor,
              size: 35.r,
            ),
          ),
        ],
      ),
      backgroundColor: Colors.grey.shade200,
      body: BlocBuilder<HomeBloc, HomeState>(
        builder: (context, state) {
          switch (state.status) {
            case HomeStatus.initLoading:
              return const Center(child: CircularProgressIndicator());
            case HomeStatus.initError:
              return Center(
                child: Text(state.error!.message),
              );
            case HomeStatus.data:
              return ListView.builder(
                controller: _controller,
                itemCount: state.leaves!.length,
                itemBuilder: (context, index) {
                  return Column(
                    children: [
                      if (index == 0)
                        AnnualLeavesCurve(
                          annual: state.leaveCount!.annual.toString(),
                          maxAnnual: state.leaveCount!.maxAnnual.toString(),
                          userName: context.read<HomeBloc>().user.employeeName,
                        ),
                      LeaveWidget(leave: state.leaves![index]),
                      if (index == state.leaves!.length - 1 && state.isLoading)
                        Padding(
                          padding: EdgeInsets.symmetric(
                            vertical: 10.h,
                            horizontal: 10.w,
                          ),
                          child: const CircularProgressIndicator(),
                        ),
                      if (index == state.leaves!.length - 1 && state.isError)
                        Padding(
                          padding: EdgeInsets.symmetric(
                            vertical: 10.h,
                            horizontal: 10.w,
                          ),
                          child: Text(state.error!.message),
                        ),
                      if (index == state.leaves!.length - 1 &&
                          state.hasReachedMax)
                        Padding(
                          padding: EdgeInsets.symmetric(
                            vertical: 10.h,
                            horizontal: 10.w,
                          ),
                          child: const Text("You've reached the last page"),
                        ),
                    ],
                  );
                },
              );
            default:
              return const SizedBox();
          }
        },
      ),
    );
  }
}
