import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:hnddes_test/app/app-color.dart';
import 'package:hnddes_test/dto/sign-in-dto.dart';
import 'package:hnddes_test/utils/dialogs.dart';

import '../../app/router.dart';
import '../../controllers/auth/auth_cubit.dart';
import '../widgets/custom-text-form-filed-auth.widget.dart';

class LoginScreen extends StatelessWidget {
  LoginScreen({super.key});

  final formKey = GlobalKey<FormState>();
  final userNameCont = TextEditingController(text: "eshemp");
  final passCont = TextEditingController(text: "Eshemp@123");

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => FocusScope.of(context).unfocus(),
      child: BlocListener<AuthCubit, AuthState>(
        listener: (context, state) {
          state.mapOrNull(
            error: (err) {
              if (!ModalRoute.of(context)!.isCurrent) {
                Navigator.pop(context);
              }
              return showErrorSnackBar(context, err.error.message);
            },
            loading: (_) => showLoadingDialog(context),
            success: (_) => Navigator.pushReplacementNamed(
              context,
              Routs.homeScreen,
            ),
          );
        },
        child: Scaffold(
          backgroundColor: Colors.grey.shade200,
          body: Form(
            key: formKey,
            child: SingleChildScrollView(
              physics: const BouncingScrollPhysics(),
              child: Column(
                children: [
                  const Image(
                    image: AssetImage('assets/auth.png'),
                  ),
                  SizedBox(height: 20.h),
                  CustomTextFormFiledAuth(
                    hintText: 'Username',
                    validator: (value) {
                      if (value == null || value.length < 4) {
                        return 'Please enter a valid username';
                      }

                      return null;
                    },
                    isSecure: null,
                    controller: userNameCont,
                  ),
                  SizedBox(height: 20.h),
                  CustomTextFormFiledAuth(
                    hintText: 'Password',
                    validator: (value) {
                      if (value == null || value.trim().isEmpty) {
                        return 'Please enter your password';
                      }
                      if (value.trim().length < 6) {
                        return 'Password should be more than 6 character';
                      }
                      return null;
                    },
                    isSecure: true,
                    controller: passCont,
                  ),
                  SizedBox(height: 20.h),
                  Padding(
                    padding: EdgeInsets.symmetric(
                      horizontal: 20.w,
                    ),
                    child: ElevatedButton(
                      style: ElevatedButton.styleFrom(
                        minimumSize: Size.fromHeight(30.h),
                        backgroundColor: AppColor.mainColor,
                        padding: EdgeInsets.all(15.r),
                        textStyle: TextStyle(
                          fontSize: 20.sp,
                          color: Colors.white,
                        ),
                      ),
                      onPressed: () {
                        final signInDto = SignInDto(
                          userName: userNameCont.text,
                          password: passCont.text,
                        );
                        context.read<AuthCubit>().login(signInDto);
                      },
                      child: Text(
                        'Login',
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 30.sp,
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
