// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import '../../models/leave.dart';
import 'leave-widget-details.dart';

class LeaveWidget extends StatelessWidget {
  const LeaveWidget({
    Key? key,
    required this.leave,
  }) : super(key: key);
  final Leave leave;

  @override
  Widget build(BuildContext context) {
    return Card(
      margin: EdgeInsets.symmetric(vertical: 5.h, horizontal: 10.w),
      child: Column(
        children: [
          LeaveWidgetDetails(
            iconData: Icons.calendar_month,
            disc: 'Applied Duration',
            iconColor: Colors.pink.shade300,
            bgColor: Colors.pink.shade100,
            widget: RichText(
              text: TextSpan(
                style: TextStyle(
                  color: Colors.black,
                  fontSize: 15.sp,
                ),
                text: leave.absenceFrom,
                children: [
                  const TextSpan(text: ' to '),
                  TextSpan(text: leave.absenceTo),
                ],
              ),
            ),
          ),
          LeaveWidgetDetails(
            iconData: Icons.message,
            disc: 'Notes',
            iconColor: Colors.lightBlue.shade300,
            bgColor: Colors.lightBlue.shade100,
            widget: Text(leave.notes ?? 'No notes'),
          ),
          LeaveWidgetDetails(
            iconData: CupertinoIcons.exclamationmark,
            disc: 'Status',
            iconColor: Colors.grey.shade800,
            bgColor: Colors.yellow.shade300,
            widget: Text(leave.statusName),
          ),
        ],
      ),
    );
  }
}
