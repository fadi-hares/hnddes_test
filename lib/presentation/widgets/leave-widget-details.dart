// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class LeaveWidgetDetails extends StatelessWidget {
  const LeaveWidgetDetails({
    Key? key,
    required this.iconData,
    required this.widget,
    required this.disc,
    required this.iconColor,
    required this.bgColor,
  }) : super(key: key);

  final IconData iconData;
  final Widget widget;

  final String disc;
  final Color iconColor;
  final Color bgColor;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(vertical: 10.h, horizontal: 10.w),
      child: Row(
        children: [
          ClipOval(
            child: Container(
              padding: EdgeInsets.all(10.r),
              color: bgColor,
              child: Icon(
                iconData,
                color: iconColor,
              ),
            ),
          ),
          SizedBox(width: 10.w),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                disc,
                style: TextStyle(
                  color: Colors.grey.shade500,
                  fontWeight: FontWeight.normal,
                  fontSize: 15.sp,
                ),
              ),
              SizedBox(height: 5.h),
              widget,
            ],
          ),
        ],
      ),
    );
  }
}
