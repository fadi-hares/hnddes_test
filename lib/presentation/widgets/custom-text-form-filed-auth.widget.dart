// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import '../../app/app-color.dart';

class CustomTextFormFiledAuth extends StatelessWidget {
  CustomTextFormFiledAuth({
    Key? key,
    required this.hintText,
    required this.validator,
    required this.isSecure,
    required this.controller,
  }) : super(key: key);
  final String hintText;
  final String? Function(String? value) validator;
  bool? isSecure;
  final TextEditingController controller;

  @override
  Widget build(BuildContext context) {
    return StatefulBuilder(
      builder: (context, setState) {
        return Padding(
          padding: EdgeInsets.symmetric(horizontal: 20.w),
          child: TextFormField(
            controller: controller,
            obscureText: isSecure ?? false,
            cursorColor: Colors.grey.shade800,
            style: TextStyle(
              color: Colors.grey.shade800,
              fontSize: 15.sp,
            ),
            decoration: InputDecoration(
              suffixIcon: isSecure != null
                  ? IconButton(
                      onPressed: () {
                        setState.call(() {
                          isSecure = !isSecure!;
                        });
                      },
                      icon: const Icon(
                        Icons.remove_red_eye,
                        color: AppColor.mainColor,
                      ),
                    )
                  : null,
              disabledBorder: const OutlineInputBorder(
                borderSide: BorderSide(
                  color: AppColor.mainColor,
                ),
              ),
              enabledBorder: const OutlineInputBorder(
                borderSide: BorderSide(
                  color: AppColor.mainColor,
                ),
              ),
              errorStyle: const TextStyle(
                color: AppColor.mainColor,
              ),
              errorBorder: const OutlineInputBorder(
                borderSide: BorderSide(
                  color: AppColor.mainColor,
                ),
              ),
              focusedErrorBorder: const OutlineInputBorder(
                borderSide: BorderSide(
                  color: AppColor.mainColor,
                ),
              ),
              focusedBorder: const OutlineInputBorder(
                borderSide: BorderSide(
                  color: AppColor.mainColor,
                ),
              ),
              border: const OutlineInputBorder(
                borderSide: BorderSide(
                  color: AppColor.mainColor,
                ),
              ),
              focusColor: AppColor.mainColor,
              labelStyle: TextStyle(
                color: AppColor.mainColor,
                fontSize: 10.sp,
              ),
              label: Text(
                hintText,
                style: TextStyle(
                  color: AppColor.mainColor,
                  fontSize: 15.sp,
                ),
              ),
            ),
            validator: validator,
          ),
        );
      },
    );
  }
}
