// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'package:equatable/equatable.dart';

import '../../app/custom-error.dart';
import '../../models/leave-count.dart';
import '../../models/leave.dart';

enum HomeStatus {
  init,
  initLoading,
  initError,
  data,
}

class HomeState extends Equatable {
  String pageNumber;
  HomeStatus status;
  bool hasReachedMax;
  CustomError? error;
  LeaveCount? leaveCount;
  List<Leave>? leaves;
  bool isLoading;
  bool isError;
  HomeState({
    required this.pageNumber,
    required this.status,
    required this.hasReachedMax,
    this.error,
    this.leaveCount,
    this.leaves,
    required this.isLoading,
    required this.isError,
  });

  factory HomeState.initial() => HomeState(
        status: HomeStatus.init,
        pageNumber: '1',
        hasReachedMax: false,
        isError: false,
        isLoading: false,
      );

  @override
  List<Object?> get props {
    return [
      pageNumber,
      status,
      hasReachedMax,
      error,
      leaveCount,
      leaves,
      isLoading,
      isError,
    ];
  }

  @override
  bool get stringify => true;

  HomeState copyWith({
    String? pageNumber,
    HomeStatus? status,
    bool? hasReachedMax,
    CustomError? error,
    LeaveCount? leaveCount,
    List<Leave>? leaves,
    bool? isLoading,
    bool? isError,
  }) {
    return HomeState(
      pageNumber: pageNumber ?? this.pageNumber,
      status: status ?? this.status,
      hasReachedMax: hasReachedMax ?? this.hasReachedMax,
      error: error ?? this.error,
      leaveCount: leaveCount ?? this.leaveCount,
      leaves: leaves ?? this.leaves,
      isLoading: isLoading ?? this.isLoading,
      isError: isError ?? this.isError,
    );
  }
}
