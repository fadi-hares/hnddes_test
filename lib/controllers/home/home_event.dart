part of 'home_bloc.dart';

@freezed
class HomeEvent with _$HomeEvent {
  const factory HomeEvent.getLeaveList() = GetLeaveList;
  const factory HomeEvent.getInitData() = GetInitData;
}
