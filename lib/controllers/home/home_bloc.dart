// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'package:bloc/bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:hnddes_test/app/custom-error.dart';
import 'package:hnddes_test/repository/get-data-repo.dart';
import 'package:hnddes_test/repository/local-repo.dart';

import '../../models/user-module.dart';
import 'home_state.dart';

part 'home_bloc.freezed.dart';
part 'home_event.dart';

class HomeBloc extends Bloc<HomeEvent, HomeState> {
  late User user;
  LocalRepo localRepo;
  GetDataRepo getDataRepo;
  HomeBloc(
    this.localRepo,
    this.getDataRepo,
  ) : super(HomeState.initial()) {
    on<GetInitData>((event, emit) async {
      emit(state.copyWith(status: HomeStatus.initLoading));
      try {
        final data = await getDataRepo.getLeaveCount(
          user.employeeId.toString(),
        );
        final leavesData = await getDataRepo.getLeaveList(
          user: user,
          pageNumber: state.pageNumber,
          pageSized: '5',
        );
        emit(
          state.copyWith(
            leaveCount: data,
            leaves: leavesData.leaves,
            status: HomeStatus.data,
          ),
        );
      } on CustomError catch (e) {
        emit(
          state.copyWith(
            error: e,
            status: HomeStatus.initError,
          ),
        );
      }
    });

    on<GetLeaveList>((event, emit) async {
      if (state.hasReachedMax) return;
      emit(state.copyWith(isLoading: true, isError: false));
      try {
        final data = await getDataRepo.getLeaveList(
          user: user,
          pageNumber: state.pageNumber,
          pageSized: '5',
        );

        final newLeavesList = [...state.leaves!, ...data.leaves];

        if (state.pageNumber == data.totalPages.toString()) {
          emit(
            state.copyWith(
              isLoading: false,
              leaves: newLeavesList,
              pageNumber: (int.parse(state.pageNumber) + 1).toString(),
              hasReachedMax: true,
            ),
          );
        } else {
          emit(
            state.copyWith(
              isLoading: false,
              leaves: newLeavesList,
              pageNumber: (int.parse(state.pageNumber) + 1).toString(),
            ),
          );
        }
      } on CustomError catch (e) {
        emit(
          state.copyWith(
            error: e,
            isLoading: false,
            isError: true,
          ),
        );
      }
    });

    user = localRepo.getUser()!;
  }
}
