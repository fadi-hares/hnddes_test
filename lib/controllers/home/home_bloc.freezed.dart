// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'home_bloc.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$HomeEvent {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() getLeaveList,
    required TResult Function() getInitData,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? getLeaveList,
    TResult? Function()? getInitData,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? getLeaveList,
    TResult Function()? getInitData,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(GetLeaveList value) getLeaveList,
    required TResult Function(GetInitData value) getInitData,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(GetLeaveList value)? getLeaveList,
    TResult? Function(GetInitData value)? getInitData,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(GetLeaveList value)? getLeaveList,
    TResult Function(GetInitData value)? getInitData,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $HomeEventCopyWith<$Res> {
  factory $HomeEventCopyWith(HomeEvent value, $Res Function(HomeEvent) then) =
      _$HomeEventCopyWithImpl<$Res, HomeEvent>;
}

/// @nodoc
class _$HomeEventCopyWithImpl<$Res, $Val extends HomeEvent>
    implements $HomeEventCopyWith<$Res> {
  _$HomeEventCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;
}

/// @nodoc
abstract class _$$GetLeaveListCopyWith<$Res> {
  factory _$$GetLeaveListCopyWith(
          _$GetLeaveList value, $Res Function(_$GetLeaveList) then) =
      __$$GetLeaveListCopyWithImpl<$Res>;
}

/// @nodoc
class __$$GetLeaveListCopyWithImpl<$Res>
    extends _$HomeEventCopyWithImpl<$Res, _$GetLeaveList>
    implements _$$GetLeaveListCopyWith<$Res> {
  __$$GetLeaveListCopyWithImpl(
      _$GetLeaveList _value, $Res Function(_$GetLeaveList) _then)
      : super(_value, _then);
}

/// @nodoc

class _$GetLeaveList implements GetLeaveList {
  const _$GetLeaveList();

  @override
  String toString() {
    return 'HomeEvent.getLeaveList()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$GetLeaveList);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() getLeaveList,
    required TResult Function() getInitData,
  }) {
    return getLeaveList();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? getLeaveList,
    TResult? Function()? getInitData,
  }) {
    return getLeaveList?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? getLeaveList,
    TResult Function()? getInitData,
    required TResult orElse(),
  }) {
    if (getLeaveList != null) {
      return getLeaveList();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(GetLeaveList value) getLeaveList,
    required TResult Function(GetInitData value) getInitData,
  }) {
    return getLeaveList(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(GetLeaveList value)? getLeaveList,
    TResult? Function(GetInitData value)? getInitData,
  }) {
    return getLeaveList?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(GetLeaveList value)? getLeaveList,
    TResult Function(GetInitData value)? getInitData,
    required TResult orElse(),
  }) {
    if (getLeaveList != null) {
      return getLeaveList(this);
    }
    return orElse();
  }
}

abstract class GetLeaveList implements HomeEvent {
  const factory GetLeaveList() = _$GetLeaveList;
}

/// @nodoc
abstract class _$$GetInitDataCopyWith<$Res> {
  factory _$$GetInitDataCopyWith(
          _$GetInitData value, $Res Function(_$GetInitData) then) =
      __$$GetInitDataCopyWithImpl<$Res>;
}

/// @nodoc
class __$$GetInitDataCopyWithImpl<$Res>
    extends _$HomeEventCopyWithImpl<$Res, _$GetInitData>
    implements _$$GetInitDataCopyWith<$Res> {
  __$$GetInitDataCopyWithImpl(
      _$GetInitData _value, $Res Function(_$GetInitData) _then)
      : super(_value, _then);
}

/// @nodoc

class _$GetInitData implements GetInitData {
  const _$GetInitData();

  @override
  String toString() {
    return 'HomeEvent.getInitData()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$GetInitData);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() getLeaveList,
    required TResult Function() getInitData,
  }) {
    return getInitData();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? getLeaveList,
    TResult? Function()? getInitData,
  }) {
    return getInitData?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? getLeaveList,
    TResult Function()? getInitData,
    required TResult orElse(),
  }) {
    if (getInitData != null) {
      return getInitData();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(GetLeaveList value) getLeaveList,
    required TResult Function(GetInitData value) getInitData,
  }) {
    return getInitData(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(GetLeaveList value)? getLeaveList,
    TResult? Function(GetInitData value)? getInitData,
  }) {
    return getInitData?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(GetLeaveList value)? getLeaveList,
    TResult Function(GetInitData value)? getInitData,
    required TResult orElse(),
  }) {
    if (getInitData != null) {
      return getInitData(this);
    }
    return orElse();
  }
}

abstract class GetInitData implements HomeEvent {
  const factory GetInitData() = _$GetInitData;
}
