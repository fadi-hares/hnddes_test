// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'package:bloc/bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:hnddes_test/app/custom-error.dart';
import 'package:hnddes_test/dto/sign-in-dto.dart';
import 'package:hnddes_test/repository/auth-repo.dart';

import '../../models/user-module.dart';

part 'auth_cubit.freezed.dart';
part 'auth_state.dart';

class AuthCubit extends Cubit<AuthState> {
  AuthCubit(
    this.authRepo,
  ) : super(const AuthState.initial());
  final AuthRepo authRepo;

  void login(SignInDto signInDto) async {
    emit(const AuthState.loading());
    try {
      final user = await authRepo.login(signInDto);
      emit(AuthState.success(user));
    } on CustomError catch (error) {
      emit(AuthState.error(error));
    }
  }
}
