// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'package:hive/hive.dart';

import '../models/user-module.dart';

class LocalRepo {
  HiveInterface hiveInterface;
  LocalRepo({
    required this.hiveInterface,
  }) {
    authBox = hiveInterface.box('auth');
  }

  late Box authBox;

  void setUser(User user) async {
    await authBox.put('user', user);
  }

  User? getUser() {
    final user = authBox.get('user');

    return user;
  }
}
