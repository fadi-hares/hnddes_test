// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'dart:developer';

import 'package:dio/dio.dart';
import 'package:hive/hive.dart';
import 'package:hnddes_test/app/custom-error.dart';
import 'package:hnddes_test/dto/sign-in-dto.dart';
import 'package:hnddes_test/models/user-module.dart';

import 'local-repo.dart';

class AuthRepo {
  LocalRepo localRepo;
  Dio dio;
  AuthRepo({
    required this.localRepo,
    required this.dio,
  });

  Future<User> login(SignInDto signInDto) async {
    try {
      final res = await dio.post(
        'Auth/login',
        data: {
          "username": signInDto.userName,
          "password": signInDto.password,
        },
      );

      if (res.statusCode != 200) {
        throw CustomError(message: 'Error happened');
      }

      final user = User.fromJson(res.data);
      localRepo.setUser(user);
      await user.save();
      return user;
    } on DioError catch (e) {
      log(e.message.toString());

      throw CustomError(message: e.message!);
    } on HiveError catch (e) {
      log(e.message.toString());
      throw CustomError(message: e.message);
    } catch (e) {
      throw CustomError(message: e.toString());
    }
  }
}
