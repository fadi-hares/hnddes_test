// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'dart:developer';

import 'package:dio/dio.dart';
import 'package:hnddes_test/app/custom-error.dart';
import 'package:hnddes_test/models/leave-count.dart';
import 'package:hnddes_test/models/leave-list-data.dart';
import 'package:hnddes_test/models/leave.dart';
import 'package:hnddes_test/models/user-module.dart';
import 'package:intl/intl.dart';

class GetDataRepo {
  Dio dio;
  GetDataRepo({
    required this.dio,
  });

  Future<LeaveCount> getLeaveCount(String employeeId) async {
    try {
      final res = await dio.get('Employee/LeaveCount/$employeeId');
      final leaveCount = LeaveCount.fromJson(res.data);
      return leaveCount;
    } on DioError catch (e) {
      throw CustomError(message: e.message!);
    } catch (e) {
      throw CustomError(message: e.toString());
    }
  }

  Future<LeaveListData> getLeaveList({
    required User user,
    required String pageNumber,
    required String pageSized,
  }) async {
    try {
      final res = await dio.get('Leave/List',
          options: Options(
            headers: {
              'companyId': user.companyID,
              'departmentId': user.departmentID,
              'employeeId': user.employeeId,
              'pageNumber': pageNumber,
              'pageSize': pageSized,
            },
          ));

      final leaves = (res.data['data']['leaves'] as List).map((e) {
        final leave = Leave.fromJson(e);
        leave.absenceFrom = utcToLocal(leave.absenceFrom);
        leave.absenceTo = utcToLocal(leave.absenceTo);
        return leave;
      }).toList();

      final data = LeaveListData(
        leaves: leaves,
        totalPages: res.data['data']['totalPages'],
      );

      return data;
    } on DioError catch (e) {
      throw CustomError(message: e.message ?? 'Error getting data');
    } catch (e) {
      log(e.toString());
      throw CustomError(message: e.toString());
    }
  }

  String utcToLocal(String utcDate) {
    final inputDate = DateFormat('yyyy-MM-dd HH:mm').parse(utcDate);
    final outputFormat = DateFormat('MMMM d, yyyy').format(inputDate);
    return outputFormat;
  }
}
