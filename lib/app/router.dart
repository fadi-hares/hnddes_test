import 'package:flutter/material.dart';
import 'package:hnddes_test/presentation/pages/login-screen.dart';
import 'package:hnddes_test/presentation/pages/second-screen.dart';

class Routs {
  static const loginScreen = '/';
  static const homeScreen = '/home';
}

Route routeGenerator(RouteSettings settings) {
  switch (settings.name) {
    case Routs.loginScreen:
      return MaterialPageRoute(
        builder: (context) => LoginScreen(),
      );
    case Routs.homeScreen:
      return MaterialPageRoute(
        builder: (context) => const SecondScreen(),
      );
    default:
      return MaterialPageRoute(
        builder: (context) => const Scaffold(
          body: Center(
            child: Text('No route found'),
          ),
        ),
      );
  }
}
