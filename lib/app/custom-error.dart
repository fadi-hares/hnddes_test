// ignore_for_file: public_member_api_docs, sort_constructors_first
class CustomError implements Exception {
  final String message;
  CustomError({
    required this.message,
  });
}
