// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'package:dio/dio.dart';
import 'package:hnddes_test/repository/local-repo.dart';

class DioFactory {
  static Dio getDio(LocalRepo localRepo) {
    String? token;
    final user = localRepo.getUser();

    if (user != null) {
      token = user.token;
    }

    Dio dio = Dio();
    dio.options = BaseOptions(
      baseUrl: 'https://staging.api.hr-portals.com/api/v1/',
      contentType: Headers.jsonContentType,
      receiveTimeout: const Duration(seconds: 10),
      connectTimeout: const Duration(seconds: 10),
      headers: {
        'Authorization': 'Bearer $token',
      },
    );
    return dio;
  }
}
