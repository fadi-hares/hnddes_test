import 'package:flutter/material.dart';

class AppColor {
  static const mainColor = Colors.deepPurple;
  static const secColor = Colors.purple;
}
