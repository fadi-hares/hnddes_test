import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:hive_flutter/adapters.dart';
import 'package:hnddes_test/app/dio-factory.dart';
import 'package:hnddes_test/app/router.dart';
import 'package:hnddes_test/controllers/home/home_bloc.dart';
import 'package:hnddes_test/repository/auth-repo.dart';
import 'package:hnddes_test/repository/get-data-repo.dart';
import 'package:hnddes_test/repository/local-repo.dart';

import 'controllers/auth/auth_cubit.dart';
import 'models/user-adapter.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Hive.initFlutter();
  Hive.registerAdapter(UserAdapter());
  await Hive.openBox('auth');

  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MultiRepositoryProvider(
      providers: [
        RepositoryProvider(
          create: (context) => LocalRepo(hiveInterface: Hive),
        ),
        RepositoryProvider(
          create: (context) {
            final dio = DioFactory.getDio(context.read());
            return GetDataRepo(dio: dio);
          },
        ),
        RepositoryProvider(
          create: (context) {
            final dio = DioFactory.getDio(context.read());
            return AuthRepo(localRepo: context.read(), dio: dio);
          },
        ),
      ],
      child: MultiBlocProvider(
        providers: [
          BlocProvider(
            create: (context) => AuthCubit(context.read()),
          ),
          BlocProvider(
            create: (context) => HomeBloc(context.read(), context.read()),
          ),
        ],
        child: ScreenUtilInit(
          builder: (context, c) {
            return const SafeArea(
              child: MaterialApp(
                title: 'Hnddes Test',
                debugShowCheckedModeBanner: false,
                onGenerateRoute: routeGenerator,
              ),
            );
          },
        ),
      ),
    );
  }
}
