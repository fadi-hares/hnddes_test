// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'leave-count.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_LeaveCount _$$_LeaveCountFromJson(Map<String, dynamic> json) =>
    _$_LeaveCount(
      maxAnnual: (json['maxAnnual'] as num).toDouble(),
      annual: json['annual'] as int,
    );

Map<String, dynamic> _$$_LeaveCountToJson(_$_LeaveCount instance) =>
    <String, dynamic>{
      'maxAnnual': instance.maxAnnual,
      'annual': instance.annual,
    };
