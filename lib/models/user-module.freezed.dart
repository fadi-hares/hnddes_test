// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'user-module.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$User {
  @HiveField(0)
  String get token => throw _privateConstructorUsedError;
  @HiveField(0)
  set token(String value) => throw _privateConstructorUsedError;
  @HiveField(1)
  String get employeeName => throw _privateConstructorUsedError;
  @HiveField(1)
  set employeeName(String value) => throw _privateConstructorUsedError;
  @HiveField(2)
  int get employeeId => throw _privateConstructorUsedError;
  @HiveField(2)
  set employeeId(int value) => throw _privateConstructorUsedError;
  @HiveField(3)
  int get companyID => throw _privateConstructorUsedError;
  @HiveField(3)
  set companyID(int value) => throw _privateConstructorUsedError;
  @HiveField(4)
  int get departmentID => throw _privateConstructorUsedError;
  @HiveField(4)
  set departmentID(int value) => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $UserCopyWith<User> get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $UserCopyWith<$Res> {
  factory $UserCopyWith(User value, $Res Function(User) then) =
      _$UserCopyWithImpl<$Res, User>;
  @useResult
  $Res call(
      {@HiveField(0) String token,
      @HiveField(1) String employeeName,
      @HiveField(2) int employeeId,
      @HiveField(3) int companyID,
      @HiveField(4) int departmentID});
}

/// @nodoc
class _$UserCopyWithImpl<$Res, $Val extends User>
    implements $UserCopyWith<$Res> {
  _$UserCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? token = null,
    Object? employeeName = null,
    Object? employeeId = null,
    Object? companyID = null,
    Object? departmentID = null,
  }) {
    return _then(_value.copyWith(
      token: null == token
          ? _value.token
          : token // ignore: cast_nullable_to_non_nullable
              as String,
      employeeName: null == employeeName
          ? _value.employeeName
          : employeeName // ignore: cast_nullable_to_non_nullable
              as String,
      employeeId: null == employeeId
          ? _value.employeeId
          : employeeId // ignore: cast_nullable_to_non_nullable
              as int,
      companyID: null == companyID
          ? _value.companyID
          : companyID // ignore: cast_nullable_to_non_nullable
              as int,
      departmentID: null == departmentID
          ? _value.departmentID
          : departmentID // ignore: cast_nullable_to_non_nullable
              as int,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$_UserCopyWith<$Res> implements $UserCopyWith<$Res> {
  factory _$$_UserCopyWith(_$_User value, $Res Function(_$_User) then) =
      __$$_UserCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {@HiveField(0) String token,
      @HiveField(1) String employeeName,
      @HiveField(2) int employeeId,
      @HiveField(3) int companyID,
      @HiveField(4) int departmentID});
}

/// @nodoc
class __$$_UserCopyWithImpl<$Res> extends _$UserCopyWithImpl<$Res, _$_User>
    implements _$$_UserCopyWith<$Res> {
  __$$_UserCopyWithImpl(_$_User _value, $Res Function(_$_User) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? token = null,
    Object? employeeName = null,
    Object? employeeId = null,
    Object? companyID = null,
    Object? departmentID = null,
  }) {
    return _then(_$_User(
      token: null == token
          ? _value.token
          : token // ignore: cast_nullable_to_non_nullable
              as String,
      employeeName: null == employeeName
          ? _value.employeeName
          : employeeName // ignore: cast_nullable_to_non_nullable
              as String,
      employeeId: null == employeeId
          ? _value.employeeId
          : employeeId // ignore: cast_nullable_to_non_nullable
              as int,
      companyID: null == companyID
          ? _value.companyID
          : companyID // ignore: cast_nullable_to_non_nullable
              as int,
      departmentID: null == departmentID
          ? _value.departmentID
          : departmentID // ignore: cast_nullable_to_non_nullable
              as int,
    ));
  }
}

/// @nodoc

@HiveType(typeId: 0, adapterName: 'UserAdapter')
class _$_User extends _User {
  _$_User(
      {@HiveField(0) required this.token,
      @HiveField(1) required this.employeeName,
      @HiveField(2) required this.employeeId,
      @HiveField(3) required this.companyID,
      @HiveField(4) required this.departmentID})
      : super._();

  @override
  @HiveField(0)
  String token;
  @override
  @HiveField(1)
  String employeeName;
  @override
  @HiveField(2)
  int employeeId;
  @override
  @HiveField(3)
  int companyID;
  @override
  @HiveField(4)
  int departmentID;

  @override
  String toString() {
    return 'User(token: $token, employeeName: $employeeName, employeeId: $employeeId, companyID: $companyID, departmentID: $departmentID)';
  }

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_UserCopyWith<_$_User> get copyWith =>
      __$$_UserCopyWithImpl<_$_User>(this, _$identity);
}

abstract class _User extends User {
  factory _User(
      {@HiveField(0) required String token,
      @HiveField(1) required String employeeName,
      @HiveField(2) required int employeeId,
      @HiveField(3) required int companyID,
      @HiveField(4) required int departmentID}) = _$_User;
  _User._() : super._();

  @override
  @HiveField(0)
  String get token;
  @HiveField(0)
  set token(String value);
  @override
  @HiveField(1)
  String get employeeName;
  @HiveField(1)
  set employeeName(String value);
  @override
  @HiveField(2)
  int get employeeId;
  @HiveField(2)
  set employeeId(int value);
  @override
  @HiveField(3)
  int get companyID;
  @HiveField(3)
  set companyID(int value);
  @override
  @HiveField(4)
  int get departmentID;
  @HiveField(4)
  set departmentID(int value);
  @override
  @JsonKey(ignore: true)
  _$$_UserCopyWith<_$_User> get copyWith => throw _privateConstructorUsedError;
}
