import 'package:freezed_annotation/freezed_annotation.dart';

part 'leave.freezed.dart';
part 'leave.g.dart';

@unfreezed
class Leave with _$Leave {
  factory Leave({
    required int id,
    required String employeeName,
    required int statusId,
    required String statusName,
    required String absenceValue,
    required String number,
    required int employeeId,
    required int typeId,
    required String absenceFrom,
    required String absenceTo,
    required String? notes,
  }) = _Leave;

  factory Leave.fromJson(Map<String, dynamic> json) => _$LeaveFromJson(json);
}
