// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'leave.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

Leave _$LeaveFromJson(Map<String, dynamic> json) {
  return _Leave.fromJson(json);
}

/// @nodoc
mixin _$Leave {
  int get id => throw _privateConstructorUsedError;
  set id(int value) => throw _privateConstructorUsedError;
  String get employeeName => throw _privateConstructorUsedError;
  set employeeName(String value) => throw _privateConstructorUsedError;
  int get statusId => throw _privateConstructorUsedError;
  set statusId(int value) => throw _privateConstructorUsedError;
  String get statusName => throw _privateConstructorUsedError;
  set statusName(String value) => throw _privateConstructorUsedError;
  String get absenceValue => throw _privateConstructorUsedError;
  set absenceValue(String value) => throw _privateConstructorUsedError;
  String get number => throw _privateConstructorUsedError;
  set number(String value) => throw _privateConstructorUsedError;
  int get employeeId => throw _privateConstructorUsedError;
  set employeeId(int value) => throw _privateConstructorUsedError;
  int get typeId => throw _privateConstructorUsedError;
  set typeId(int value) => throw _privateConstructorUsedError;
  String get absenceFrom => throw _privateConstructorUsedError;
  set absenceFrom(String value) => throw _privateConstructorUsedError;
  String get absenceTo => throw _privateConstructorUsedError;
  set absenceTo(String value) => throw _privateConstructorUsedError;
  String? get notes => throw _privateConstructorUsedError;
  set notes(String? value) => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $LeaveCopyWith<Leave> get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $LeaveCopyWith<$Res> {
  factory $LeaveCopyWith(Leave value, $Res Function(Leave) then) =
      _$LeaveCopyWithImpl<$Res, Leave>;
  @useResult
  $Res call(
      {int id,
      String employeeName,
      int statusId,
      String statusName,
      String absenceValue,
      String number,
      int employeeId,
      int typeId,
      String absenceFrom,
      String absenceTo,
      String? notes});
}

/// @nodoc
class _$LeaveCopyWithImpl<$Res, $Val extends Leave>
    implements $LeaveCopyWith<$Res> {
  _$LeaveCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? id = null,
    Object? employeeName = null,
    Object? statusId = null,
    Object? statusName = null,
    Object? absenceValue = null,
    Object? number = null,
    Object? employeeId = null,
    Object? typeId = null,
    Object? absenceFrom = null,
    Object? absenceTo = null,
    Object? notes = freezed,
  }) {
    return _then(_value.copyWith(
      id: null == id
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as int,
      employeeName: null == employeeName
          ? _value.employeeName
          : employeeName // ignore: cast_nullable_to_non_nullable
              as String,
      statusId: null == statusId
          ? _value.statusId
          : statusId // ignore: cast_nullable_to_non_nullable
              as int,
      statusName: null == statusName
          ? _value.statusName
          : statusName // ignore: cast_nullable_to_non_nullable
              as String,
      absenceValue: null == absenceValue
          ? _value.absenceValue
          : absenceValue // ignore: cast_nullable_to_non_nullable
              as String,
      number: null == number
          ? _value.number
          : number // ignore: cast_nullable_to_non_nullable
              as String,
      employeeId: null == employeeId
          ? _value.employeeId
          : employeeId // ignore: cast_nullable_to_non_nullable
              as int,
      typeId: null == typeId
          ? _value.typeId
          : typeId // ignore: cast_nullable_to_non_nullable
              as int,
      absenceFrom: null == absenceFrom
          ? _value.absenceFrom
          : absenceFrom // ignore: cast_nullable_to_non_nullable
              as String,
      absenceTo: null == absenceTo
          ? _value.absenceTo
          : absenceTo // ignore: cast_nullable_to_non_nullable
              as String,
      notes: freezed == notes
          ? _value.notes
          : notes // ignore: cast_nullable_to_non_nullable
              as String?,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$_LeaveCopyWith<$Res> implements $LeaveCopyWith<$Res> {
  factory _$$_LeaveCopyWith(_$_Leave value, $Res Function(_$_Leave) then) =
      __$$_LeaveCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {int id,
      String employeeName,
      int statusId,
      String statusName,
      String absenceValue,
      String number,
      int employeeId,
      int typeId,
      String absenceFrom,
      String absenceTo,
      String? notes});
}

/// @nodoc
class __$$_LeaveCopyWithImpl<$Res> extends _$LeaveCopyWithImpl<$Res, _$_Leave>
    implements _$$_LeaveCopyWith<$Res> {
  __$$_LeaveCopyWithImpl(_$_Leave _value, $Res Function(_$_Leave) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? id = null,
    Object? employeeName = null,
    Object? statusId = null,
    Object? statusName = null,
    Object? absenceValue = null,
    Object? number = null,
    Object? employeeId = null,
    Object? typeId = null,
    Object? absenceFrom = null,
    Object? absenceTo = null,
    Object? notes = freezed,
  }) {
    return _then(_$_Leave(
      id: null == id
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as int,
      employeeName: null == employeeName
          ? _value.employeeName
          : employeeName // ignore: cast_nullable_to_non_nullable
              as String,
      statusId: null == statusId
          ? _value.statusId
          : statusId // ignore: cast_nullable_to_non_nullable
              as int,
      statusName: null == statusName
          ? _value.statusName
          : statusName // ignore: cast_nullable_to_non_nullable
              as String,
      absenceValue: null == absenceValue
          ? _value.absenceValue
          : absenceValue // ignore: cast_nullable_to_non_nullable
              as String,
      number: null == number
          ? _value.number
          : number // ignore: cast_nullable_to_non_nullable
              as String,
      employeeId: null == employeeId
          ? _value.employeeId
          : employeeId // ignore: cast_nullable_to_non_nullable
              as int,
      typeId: null == typeId
          ? _value.typeId
          : typeId // ignore: cast_nullable_to_non_nullable
              as int,
      absenceFrom: null == absenceFrom
          ? _value.absenceFrom
          : absenceFrom // ignore: cast_nullable_to_non_nullable
              as String,
      absenceTo: null == absenceTo
          ? _value.absenceTo
          : absenceTo // ignore: cast_nullable_to_non_nullable
              as String,
      notes: freezed == notes
          ? _value.notes
          : notes // ignore: cast_nullable_to_non_nullable
              as String?,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_Leave implements _Leave {
  _$_Leave(
      {required this.id,
      required this.employeeName,
      required this.statusId,
      required this.statusName,
      required this.absenceValue,
      required this.number,
      required this.employeeId,
      required this.typeId,
      required this.absenceFrom,
      required this.absenceTo,
      required this.notes});

  factory _$_Leave.fromJson(Map<String, dynamic> json) =>
      _$$_LeaveFromJson(json);

  @override
  int id;
  @override
  String employeeName;
  @override
  int statusId;
  @override
  String statusName;
  @override
  String absenceValue;
  @override
  String number;
  @override
  int employeeId;
  @override
  int typeId;
  @override
  String absenceFrom;
  @override
  String absenceTo;
  @override
  String? notes;

  @override
  String toString() {
    return 'Leave(id: $id, employeeName: $employeeName, statusId: $statusId, statusName: $statusName, absenceValue: $absenceValue, number: $number, employeeId: $employeeId, typeId: $typeId, absenceFrom: $absenceFrom, absenceTo: $absenceTo, notes: $notes)';
  }

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_LeaveCopyWith<_$_Leave> get copyWith =>
      __$$_LeaveCopyWithImpl<_$_Leave>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_LeaveToJson(
      this,
    );
  }
}

abstract class _Leave implements Leave {
  factory _Leave(
      {required int id,
      required String employeeName,
      required int statusId,
      required String statusName,
      required String absenceValue,
      required String number,
      required int employeeId,
      required int typeId,
      required String absenceFrom,
      required String absenceTo,
      required String? notes}) = _$_Leave;

  factory _Leave.fromJson(Map<String, dynamic> json) = _$_Leave.fromJson;

  @override
  int get id;
  set id(int value);
  @override
  String get employeeName;
  set employeeName(String value);
  @override
  int get statusId;
  set statusId(int value);
  @override
  String get statusName;
  set statusName(String value);
  @override
  String get absenceValue;
  set absenceValue(String value);
  @override
  String get number;
  set number(String value);
  @override
  int get employeeId;
  set employeeId(int value);
  @override
  int get typeId;
  set typeId(int value);
  @override
  String get absenceFrom;
  set absenceFrom(String value);
  @override
  String get absenceTo;
  set absenceTo(String value);
  @override
  String? get notes;
  set notes(String? value);
  @override
  @JsonKey(ignore: true)
  _$$_LeaveCopyWith<_$_Leave> get copyWith =>
      throw _privateConstructorUsedError;
}
