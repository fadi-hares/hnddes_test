// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'leave.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_Leave _$$_LeaveFromJson(Map<String, dynamic> json) => _$_Leave(
      id: json['id'] as int,
      employeeName: json['employeeName'] as String,
      statusId: json['statusId'] as int,
      statusName: json['statusName'] as String,
      absenceValue: json['absenceValue'] as String,
      number: json['number'] as String,
      employeeId: json['employeeId'] as int,
      typeId: json['typeId'] as int,
      absenceFrom: json['absenceFrom'] as String,
      absenceTo: json['absenceTo'] as String,
      notes: json['notes'] as String?,
    );

Map<String, dynamic> _$$_LeaveToJson(_$_Leave instance) => <String, dynamic>{
      'id': instance.id,
      'employeeName': instance.employeeName,
      'statusId': instance.statusId,
      'statusName': instance.statusName,
      'absenceValue': instance.absenceValue,
      'number': instance.number,
      'employeeId': instance.employeeId,
      'typeId': instance.typeId,
      'absenceFrom': instance.absenceFrom,
      'absenceTo': instance.absenceTo,
      'notes': instance.notes,
    };
