// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'leave-list-data.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$LeaveListData {
  List<Leave> get leaves => throw _privateConstructorUsedError;
  int get totalPages => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $LeaveListDataCopyWith<LeaveListData> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $LeaveListDataCopyWith<$Res> {
  factory $LeaveListDataCopyWith(
          LeaveListData value, $Res Function(LeaveListData) then) =
      _$LeaveListDataCopyWithImpl<$Res, LeaveListData>;
  @useResult
  $Res call({List<Leave> leaves, int totalPages});
}

/// @nodoc
class _$LeaveListDataCopyWithImpl<$Res, $Val extends LeaveListData>
    implements $LeaveListDataCopyWith<$Res> {
  _$LeaveListDataCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? leaves = null,
    Object? totalPages = null,
  }) {
    return _then(_value.copyWith(
      leaves: null == leaves
          ? _value.leaves
          : leaves // ignore: cast_nullable_to_non_nullable
              as List<Leave>,
      totalPages: null == totalPages
          ? _value.totalPages
          : totalPages // ignore: cast_nullable_to_non_nullable
              as int,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$_LeaveListDataCopyWith<$Res>
    implements $LeaveListDataCopyWith<$Res> {
  factory _$$_LeaveListDataCopyWith(
          _$_LeaveListData value, $Res Function(_$_LeaveListData) then) =
      __$$_LeaveListDataCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({List<Leave> leaves, int totalPages});
}

/// @nodoc
class __$$_LeaveListDataCopyWithImpl<$Res>
    extends _$LeaveListDataCopyWithImpl<$Res, _$_LeaveListData>
    implements _$$_LeaveListDataCopyWith<$Res> {
  __$$_LeaveListDataCopyWithImpl(
      _$_LeaveListData _value, $Res Function(_$_LeaveListData) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? leaves = null,
    Object? totalPages = null,
  }) {
    return _then(_$_LeaveListData(
      leaves: null == leaves
          ? _value._leaves
          : leaves // ignore: cast_nullable_to_non_nullable
              as List<Leave>,
      totalPages: null == totalPages
          ? _value.totalPages
          : totalPages // ignore: cast_nullable_to_non_nullable
              as int,
    ));
  }
}

/// @nodoc

class _$_LeaveListData implements _LeaveListData {
  _$_LeaveListData(
      {required final List<Leave> leaves, required this.totalPages})
      : _leaves = leaves;

  final List<Leave> _leaves;
  @override
  List<Leave> get leaves {
    if (_leaves is EqualUnmodifiableListView) return _leaves;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(_leaves);
  }

  @override
  final int totalPages;

  @override
  String toString() {
    return 'LeaveListData(leaves: $leaves, totalPages: $totalPages)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_LeaveListData &&
            const DeepCollectionEquality().equals(other._leaves, _leaves) &&
            (identical(other.totalPages, totalPages) ||
                other.totalPages == totalPages));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType, const DeepCollectionEquality().hash(_leaves), totalPages);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_LeaveListDataCopyWith<_$_LeaveListData> get copyWith =>
      __$$_LeaveListDataCopyWithImpl<_$_LeaveListData>(this, _$identity);
}

abstract class _LeaveListData implements LeaveListData {
  factory _LeaveListData(
      {required final List<Leave> leaves,
      required final int totalPages}) = _$_LeaveListData;

  @override
  List<Leave> get leaves;
  @override
  int get totalPages;
  @override
  @JsonKey(ignore: true)
  _$$_LeaveListDataCopyWith<_$_LeaveListData> get copyWith =>
      throw _privateConstructorUsedError;
}
