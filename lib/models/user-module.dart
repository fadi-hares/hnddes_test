import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:hive_flutter/adapters.dart';

part 'user-module.freezed.dart';

@unfreezed
class User extends HiveObject with _$User {
  User._();

  @HiveType(typeId: 0, adapterName: 'UserAdapter')
  factory User({
    @HiveField(0) required String token,
    @HiveField(1) required String employeeName,
    @HiveField(2) required int employeeId,
    @HiveField(3) required int companyID,
    @HiveField(4) required int departmentID,
  }) = _User;

  factory User.fromJson(Map<String, dynamic> json) {
    final data = json['data'];
    return User(
      token: data['accessToken'],
      employeeId: data['user']['employee']['id'],
      employeeName: data['user']['employee']['fullName'],
      companyID: data['user']['employee']['company']['id'],
      departmentID: data['user']['employee']['department']['id'],
    );
  }
}
