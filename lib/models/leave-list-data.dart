import 'package:freezed_annotation/freezed_annotation.dart';

import 'leave.dart';

part 'leave-list-data.freezed.dart';

@freezed
class LeaveListData with _$LeaveListData {
  factory LeaveListData({
    required List<Leave> leaves,
    required int totalPages,
  }) = _LeaveListData;
}
