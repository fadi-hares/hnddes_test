import 'package:hive/hive.dart';
import 'package:hnddes_test/models/user-module.dart';

class UserAdapter extends TypeAdapter<User> {
  @override
  User read(BinaryReader reader) {
    return User(
      token: reader.readString(),
      employeeName: reader.readString(),
      employeeId: reader.readInt(),
      companyID: reader.readInt(),
      departmentID: reader.readInt(),
    );
  }

  @override
  final typeId = 0;

  @override
  void write(BinaryWriter writer, User obj) {
    writer.writeString(obj.token);
    writer.writeString(obj.employeeName);
    writer.writeInt(obj.companyID);
    writer.writeInt(obj.employeeId);
    writer.writeInt(obj.departmentID);
  }
}
