import 'package:freezed_annotation/freezed_annotation.dart';

part 'leave-count.freezed.dart';
part 'leave-count.g.dart';

@freezed
class LeaveCount with _$LeaveCount {
  const factory LeaveCount({
    required double maxAnnual,
    required int annual,
  }) = _LeaveCount;

  factory LeaveCount.fromJson(Map<String, dynamic> json) =>
      _$LeaveCountFromJson(json['data']);
}
