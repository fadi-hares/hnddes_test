// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'leave-count.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

LeaveCount _$LeaveCountFromJson(Map<String, dynamic> json) {
  return _LeaveCount.fromJson(json);
}

/// @nodoc
mixin _$LeaveCount {
  double get maxAnnual => throw _privateConstructorUsedError;
  int get annual => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $LeaveCountCopyWith<LeaveCount> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $LeaveCountCopyWith<$Res> {
  factory $LeaveCountCopyWith(
          LeaveCount value, $Res Function(LeaveCount) then) =
      _$LeaveCountCopyWithImpl<$Res, LeaveCount>;
  @useResult
  $Res call({double maxAnnual, int annual});
}

/// @nodoc
class _$LeaveCountCopyWithImpl<$Res, $Val extends LeaveCount>
    implements $LeaveCountCopyWith<$Res> {
  _$LeaveCountCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? maxAnnual = null,
    Object? annual = null,
  }) {
    return _then(_value.copyWith(
      maxAnnual: null == maxAnnual
          ? _value.maxAnnual
          : maxAnnual // ignore: cast_nullable_to_non_nullable
              as double,
      annual: null == annual
          ? _value.annual
          : annual // ignore: cast_nullable_to_non_nullable
              as int,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$_LeaveCountCopyWith<$Res>
    implements $LeaveCountCopyWith<$Res> {
  factory _$$_LeaveCountCopyWith(
          _$_LeaveCount value, $Res Function(_$_LeaveCount) then) =
      __$$_LeaveCountCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({double maxAnnual, int annual});
}

/// @nodoc
class __$$_LeaveCountCopyWithImpl<$Res>
    extends _$LeaveCountCopyWithImpl<$Res, _$_LeaveCount>
    implements _$$_LeaveCountCopyWith<$Res> {
  __$$_LeaveCountCopyWithImpl(
      _$_LeaveCount _value, $Res Function(_$_LeaveCount) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? maxAnnual = null,
    Object? annual = null,
  }) {
    return _then(_$_LeaveCount(
      maxAnnual: null == maxAnnual
          ? _value.maxAnnual
          : maxAnnual // ignore: cast_nullable_to_non_nullable
              as double,
      annual: null == annual
          ? _value.annual
          : annual // ignore: cast_nullable_to_non_nullable
              as int,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_LeaveCount implements _LeaveCount {
  const _$_LeaveCount({required this.maxAnnual, required this.annual});

  factory _$_LeaveCount.fromJson(Map<String, dynamic> json) =>
      _$$_LeaveCountFromJson(json);

  @override
  final double maxAnnual;
  @override
  final int annual;

  @override
  String toString() {
    return 'LeaveCount(maxAnnual: $maxAnnual, annual: $annual)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_LeaveCount &&
            (identical(other.maxAnnual, maxAnnual) ||
                other.maxAnnual == maxAnnual) &&
            (identical(other.annual, annual) || other.annual == annual));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hash(runtimeType, maxAnnual, annual);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_LeaveCountCopyWith<_$_LeaveCount> get copyWith =>
      __$$_LeaveCountCopyWithImpl<_$_LeaveCount>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_LeaveCountToJson(
      this,
    );
  }
}

abstract class _LeaveCount implements LeaveCount {
  const factory _LeaveCount(
      {required final double maxAnnual,
      required final int annual}) = _$_LeaveCount;

  factory _LeaveCount.fromJson(Map<String, dynamic> json) =
      _$_LeaveCount.fromJson;

  @override
  double get maxAnnual;
  @override
  int get annual;
  @override
  @JsonKey(ignore: true)
  _$$_LeaveCountCopyWith<_$_LeaveCount> get copyWith =>
      throw _privateConstructorUsedError;
}
